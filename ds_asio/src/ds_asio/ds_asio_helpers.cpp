//
// Created by ivaughn on 5/19/21.
//

#include "ds_asio/ds_asio_helpers.h"

namespace ds_asio {

MatchFunction parseMatchFunction(const ros::NodeHandle& nh, const std::string& conn_name) {
  std::string myMatch;
  nh.param<std::string>(ros::this_node::getName() + "/" + conn_name + "/matcher", myMatch, "match_char");
  ROS_INFO_STREAM(ros::this_node::getName() <<"|" <<conn_name <<": "
                                            <<"Matcher: " << myMatch);
  if (!myMatch.compare("match_char"))
  {
    // char delimiter;
    std::string hexAscii;
    nh.param<std::string>(ros::this_node::getName() + "/" + conn_name + "/delimiter", hexAscii, "0A");
    uint8_t delimiter;
    if (!sscanf(hexAscii.c_str(), "%hhX", &delimiter))
    {
      ROS_FATAL_STREAM(ros::this_node::getName() <<"|" <<conn_name <<": "
                                                 <<"Unable to create delimiter from string: " << hexAscii);
      ROS_BREAK();
    }
    ROS_INFO("%s|%s: Hex ascii (dec): %d hex: %x", ros::this_node::getName().c_str(), conn_name.c_str(), delimiter, delimiter);
    return match_char(static_cast<char>(delimiter));
  }
  else if (!myMatch.compare("match_header_length"))
  {
    int length;
    nh.param<int>(ros::this_node::getName() + "/" + conn_name + "/length", length, 833);
    std::string hexAscii;
    nh.param<std::string>(ros::this_node::getName() + "/" + conn_name + "/header", hexAscii, "7F7F");
    std::vector<unsigned char> myHeader;
    for (int i = 0; i < hexAscii.length(); i += 2)
    {
      std::string byteString = hexAscii.substr(i, 2);
      unsigned int myByte;
      sscanf(byteString.c_str(), "%X", &myByte);
      myHeader.push_back((unsigned char)myByte);
    }
    return match_header_length(myHeader, length);
  }
  else if (!myMatch.compare("match_header_read_length"))
  {
    std::string hexAscii;
    nh.param<std::string>(ros::this_node::getName() + "/" + conn_name + "/header", hexAscii, "7F7F");
    std::vector<unsigned char> myHeader;
    for (int i = 0; i < hexAscii.length(); i += 2)
    {
      std::string byteString = hexAscii.substr(i, 2);
      unsigned int myByte;
      sscanf(byteString.c_str(), "%X", &myByte);
      myHeader.push_back((unsigned char)myByte);
    }
    int length_location_bytes;
    nh.param<int>(ros::this_node::getName() + "/" + conn_name + "/length_location_bytes", length_location_bytes, 2);
    int length_field_bytes;
    nh.param<int>(ros::this_node::getName() + "/" + conn_name + "/length_field_bytes", length_field_bytes, 2);
    bool is_msb_first;
    nh.param<bool>(ros::this_node::getName() + "/" + conn_name + "/is_msb_first", is_msb_first, true);
    int add_to_length;
    nh.param<int>(ros::this_node::getName() + "/" + conn_name + "/add_to_length", add_to_length, 0);
    int max_length;
    nh.param<int>(ros::this_node::getName() + "/" + conn_name + "/max_length", max_length, 512);

    return match_header_read_length(myHeader, length_location_bytes, length_field_bytes, is_msb_first,
                                         add_to_length, max_length);
  }
  else if (!myMatch.compare("match_multi_header_length"))
  {
    std::vector<std::string> header_strs;
    std::vector<std::vector<unsigned char>> headers;
    std::vector<int> lengths;
    nh.param(ros::this_node::getName() + "/" + conn_name + "/lengths", lengths, lengths);
    nh.param(ros::this_node::getName() + "/" + conn_name + "/headers", header_strs, header_strs);

    for (const auto hdr_str : header_strs)
    {
      std::vector<unsigned char> hdr;
      for (int i = 0; i < hdr_str.length(); i += 2)
      {
        std::string byteString = hdr_str.substr(i, 2);
        unsigned int myByte;
        sscanf(byteString.c_str(), "%X", &myByte);
        hdr.push_back((unsigned char)myByte);
      }
      headers.push_back(hdr);
    }
    return match_multi_header_length(headers, lengths);
  }
  else if (!myMatch.compare("match_header_pd0"))
  {
    return match_header_pd0();
  }
  else if (!myMatch.compare("passthrough"))
  {
    return passthrough();
  }

  // Not really sure what to do if this fails.  Passthrough I guess?
  return passthrough();
}

} // namespace ds_asio