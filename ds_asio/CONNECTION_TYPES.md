# Introduction

DsConnection has several subclasses to support different I/O methods.  Each connection has a name, and all of its
parameters, topics, etc. are defined in the namespace `node_namespace/connection_name/`.  

## Common Behaviors

All DsConnections can publish their raw I/O using the `ds_core_msgs::RawData` ROS type.  These messages are published
on the topic `node_namespace/connection_name/raw`.  

The following parameters are common to all connections:

* `publish_raw` Defaults to "true".  Set to "false" to disable publishing raw data on the raw data topic.  This is
not recommended, but may be a useful optimization for certain high-throughput connections.

* A matcher function may be specified for Serial and TCP connections.  The matcher function assembles individual bytes or subpackets provided by the I/O layer into a single packet.  Without a matcher, these types of connections would commonly trigger callbacks with incomplete callbacks.  Matcher functions can be quite complex, and it is possible to provide custom matcher functions at runtime through the C++ interface.  More information is provided in a separate section below.

## UDP Connection

Type String: "UDP"

The DsUdp connection class connects to data via User Datagram Packets (UDP) over an IP link.  Each UDP message 
generates a single callback.  Options are as follows.

* `buffer_size`  Size of the input buffer to use.  Messages longer than this value may be dropped or truncated.
Defaults to 1024 bytes.  This is the most common cause of incomplete messages via UDP.

* `udp_rx`  UDP receive port.  It is strongly recommended to use ports in the "registered" range (between 1024 
and 49151) when configuring new systems.

* `udp_tx`  Port to transmit UDP datagrams to.  It is strongly recommended to use ports in the "registered" range 
(between 1024 and 49151) when configuring new systems.

* `udp_address` The IP address to use.  If a multicast address is specified, the connection will receive on that 
multicast address.  If a hostname, host IP address, or broadcast address is specified then all outgoing traffic will
be directed to that IP.

## Serial Connection

Type string: "SERIAL"

The DsSerial connection class provides interfacing to an RS232-style serial link through a file descriptor.  Standard
options are provided for the usual RS232 arguments, including baudrate, parity, etc.  Unlike UDP connections, serial 
file descriptors do not provide a straightforward way to break up messages.  Instead, a matcher function is used
to generate one callback with a complete packet or message of serial data.  Several standard matcher functions are
provided (see "Matcher Functions").  Matcher functions define their own additional parameters.  Drivers may also 
specify their own matcher function via the C++ API.

The options are as follows:

* `port` The file descriptor to use.  Probably "/dev/ttySOMETHING"

* `baud` The baudrate to use.  Defaults to 9600 and must be a valid baudrate.  Probably one of 9600, 19200, 38400,
 57600, or 115200.
 
* `data_bits` Number of data bits in the serial character.  Defaults to 8, which is almost always the right number.

* `stop_bits`  Sets the number of stop bits on the serial port.  Either "1" or "2".

* `parity` A string specifying parity options.  One of "none", "even", or "odd".

* `matcher` The matcher function to use.  Matcher functions are described separately.

Hardware flow control is not yet supported.


## TCP Client Connection

Type string: "TCPCLIENT"

The TCP Client Connection connects to a TCP server and presents any data coming in on the socket via a callback.
No matcher function is employed.  TCP connects frequently cannot detect when the connection is lost, so this 
connection class includes a timeout.  If no data is received before the timeout expires, the socket is closed and
re-opened.  Up to five attempts to re-open the socket will be made 10 seconds apart.

Available options are:

* `tcp_address` The address of the server to connect to.

* `tcp_port` The port of the server to connect to.

* `timeout_sec` The period of the no-data timeout to initation a close/reconnect.  Defaults to 30 seconds.

* `buffer_size` The maximum buffer size of a single message, in bytes.  Messages bigger than this may be dropped or
 truncated. The largest possible size permitted by TCP is 65535 bytes.  Defaults to 1024 bytes.

## ROS Raw Connection

Type string "ROSRAW"

The RosRaw connection allows the user to replay previously-recorded raw data into a DsConnection for testing.  Although
primarily used as an input, the connection does support an output topic as well.  Options are:

* `topic_rx` The topic to listen on.  Any message with the data direction set to "in" will trigger a callback in the 
driver.

* `topic_tx` The topic to transmit outgoing data to.  This is redundant with the raw I/O topic, but may be useful for
testing or something.

# Matcher Functions

Matcher functions take a stream of incoming data and separate out individual chunks of data for parsing.  Matcher functions may be specified for any type of connection, but are completely ignored for ROSRAW and UDP connections.  TCPCLIENT connections don't load matcher functions from rosparam, but do use them if specified via the API

## Standard Serial Matcher Functions

The following matchers are available for SERIAL connections:

### Match Char
This matcher creates a new packet whenever the specified character is seen.  This is the most commonly-used matcher, and works well with ascii formats.  Formats terminated with a `\r` or `\n`, such as NMEA, should use this matcher.

Type string "match_char"

* `delimiter` The character to create a packet on, in hex.  Commonly "0A" (for `\n`) or "0D" (for `\r`).  See also: `man ascii`

### Match Header Length

This matcher creates a new packet by looking for a specific start-of-message delimiter and reading a specified number of bytes.

Type string "match_header_length"

* `header` The header to look for, in ascii.  May be multiple bytes
* `length` The length to read

### Match Header Read Length

This matcher identifies the start of a packet based on a header, reads a length from the message, and uses that length to break the packet. 

Type string "match_header_read_length"

* `header` The header to look for, in ascii.  May be multiple bytes.  Defaults to "7F7F"
* `length_location_bytes` Offset from the header to the length field to use.  Defaults to 2.
* `length_field_bytes` Length of the length field, in bytes.  Defaults to 2
* `is_msb_first` Endianness of the length field.  Defaults to "false"
* `add_to_length` Extra bytes to read added to the length.  Useful to adjust the length field in the message for headers or footers/checksums that may or may not be included in the specified length value.  May be negative.
* `max_length` A maximum length permissible for messages.  Used as a sanity check to reject absurd lengths caused by garbled data.  Defaults to 512

### Match Multi Header Length

Same as "match header length", but allows use of multiple header/length pairs.

* `header` The header to look for, in ascii.  May be multiple bytes.  An array.
* `length` The length to read.  An array.  Must be same lenth as `header`

### Match Header PD0

Custom matcher to match RDI PD0 datagrams.  Superseeded by "match_header_read_length".  DEPRECATED.  No options.  Including sensor-specific matchers in the base library is also considered an anti-pattern, so this is a strong candidate for removal.

Type string "match_header_pd0"

### Passthrough

Matches on every byte.  No options.

Type string "passthrough"

## Custom matcher functions

Sensors that cannot be packetized by the `match_char` or `match_header_read_length` are unusual, but not uncommon.  These sensors may require custom matchers.

A custom matcher may be specified by calling the following function for either the DsTcpClient or DsSerial classes:

```
typedef boost::asio::buffers_iterator<boost::asio::streambuf::const_buffers_type> iterator;
  void set_matcher(boost::function<std::pair<iterator, bool>(iterator, iterator)> matchFunction);
```

This matcher will forcibly override any matcher specified in rosparam, including any parameters.  The matcher is passed to Boost's `async_read_until` function, documented here: https://www.boost.org/doc/libs/1_71_0/doc/html/boost_asio/reference/async_read_until.html

An example of a particiuarly sophisticated matcher function may be found in the UBlox GPS node (https://bitbucket.org/whoidsl/ublox_rtk/src/master/src/ublox_rtk/ublox_matcher.h)  Matcher functions are especially critical, as they produce errors that corrupt the raw logs and may impede both introspection and testing candidate fixes.  Unit testing is strongly recommended, but the boost documentation on how to do this isn't always straightforward.  The UBlox node includes an example of how to do this as well (https://bitbucket.org/whoidsl/ublox_rtk/src/master/test/test_matcher.cpp).










