# QueuedActionlib: actionlib - with queues

The `actionlib` package only provides `SimpleServer` with preemptable goals.
This package provides `QueuedActionServer` with the ability to queue
multiple goals in a FIFO manor, with invidually set timeouts
