/**
* Copyright 2025 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef QUEUED_ACTION_SERVER_H
#define QUEUED_ACTION_SERVER_H

#include <actionlib/server/action_server.h>
#include <ros/duration.h>
#include <ros/timer.h>

#include <list>
#include <functional>


namespace queued_actionlib
{
/// An actionlib server with a FIFO-like queue
///
/// Unlike the SimpleServer provided by the actionlib package, this QueuedActionServer
/// can queue up multiple goals and act on them one at a time in a first-come, first
/// served manor.
template <class ActionSpec>
class QueuedActionServer {

public:
  /// The server goal handle type 
  using GoalHandle = actionlib::ServerGoalHandle<ActionSpec>;
  /// The actual goal type
  using Goal = typename ActionSpec::_action_goal_type::_goal_type;
  /// A convenience type holing a shared pointer to a constant goal
  using ConstGoalPtr = boost::shared_ptr<const Goal>;
  /// The restult type of the action spec
  using Result = typename ActionSpec::_action_result_type::_result_type;
  
  /// The server's timeout function
  ///
  /// Called if the action times out and allows for subclesses to
  /// react. The result type will be passed to GoalHandle::setAborted call.
  using TimeoutFunction = std::function<void(const Goal&, Result&)>;

  /// The server's validation function
  ///
  /// Can be used to reject new goals from ever entering the queue
  /// (e.g. if the goal is malformed, or impossible at the moment)
  ///
  /// If the ValidationFunction returns true the goal will be accepted
  /// and added to the queue.  If the function returns false the goal
  /// will be rejected.
  ///
  /// The result value will be passed to GoalHandle::setRejected
  using ValidationFunction = std::function<bool(const Goal&, Result&)>;

  /// The server's activation function
  ///
  /// Used take action on the goal once it becomes active.  For example,
  /// if the server is used to queue communications between this node and
  /// some network process, the activation function can be used to 
  /// construct the required packet and send it on the wire once
  /// the goal becomes active.
  ///
  /// If the activation function returns false, the goal will be
  /// aborted with GoalHandle::setAborted(result).  If true,
  /// the goal is set active with GoalHandle::setAccepted()
  ///
  /// The timeout_duration parameter is used to set the timeout for
  /// goal completion.  Using a ros::Duration <= 0 disables the
  /// timeout. 
  using ActivationFunction = std::function<bool(const Goal&, Result&, ros::Duration& timeout_duration)>;


  /// Create a new QueuedActionServer
  ///
  /// nh: ros::NodeHandle
  /// name: namespace of the actionlib topics
  /// max_queue_size: maximum number of pending goals
  ///
  /// By default, the QueuedActionServer uses a timeout of 5 seconds, a validation function
  /// that accepts all goals, and an activation function that uses the default timeout and
  /// always returns true.
  QueuedActionServer(ros::NodeHandle nh, std::string name, std::size_t max_queue_size = 10)
    : server(nh, name, false)
    , max_queued_actions(max_queue_size)
    , timer(nh.createTimer(timeout, std::bind(&QueuedActionServer<ActionSpec>::timeoutCallback, this, std::placeholders::_1), true, false))
  {
    server.registerGoalCallback(std::bind(&QueuedActionServer::queueNewGoalHandle, this, std::placeholders::_1));
    server.registerCancelCallback(std::bind(&QueuedActionServer::cancelCallback, this, std::placeholders::_1));
  }

  /// Start the action server.
  auto start() -> void {
    server.start();
  }
  
  /// Returns true if a goal is active
  auto goalIsActive() const noexcept -> bool {
    return !action_list.empty();
  }

  /// Return the number of active and pending goals
  auto numActive() const noexcept -> std::size_t {
    return action_list.size();
  }

  /// Returns a shared pointer to the active goal, or an empty shared pointer
  /// if no goals are active
  auto activeGoal() const noexcept -> ConstGoalPtr {
    return goalIsActive() ? action_list.front().getGoal() : ConstGoalPtr{};
  }

  /// Mark the current goal as having finished successfully.
  ///
  /// The provided result and message will be passed to GoalHandle::setSucceeded
  ///
  /// Starts the next goal if the queue is not empty
  auto setSucceeded(Result result = {}, std::string message = {}) -> void {
    if (goalIsActive()) {
      timer.stop();
      auto gh = std::move(action_list.front());
      action_list.pop_front();
      gh.setSucceeded(result, message);
      processNextGoal();
    }
  }

  /// Mark the current goal as finished but not successful.
  ///
  /// The provided result and message will be provided to GoalHandle::setAborted
  ///
  /// Starts the next goal if the queue is not empty
  auto setAborted(Result result = {}, std::string message = {}) -> void {
    if (goalIsActive()) {
      timer.stop();
      auto gh = std::move(action_list.front());
      action_list.pop_front();
      gh.setAborted(result, message);
      processNextGoal();
    }
  }

  /// Set a validation function for the server
  auto setValidationFunction(ValidationFunction f) -> void {
    validate = f;
  }

  /// Set an activation function fot the server
  auto setActivationFunction(ActivationFunction f) -> void {
    activate = f;
  }

  /// Set a timeout function for the server
  auto setTimeoutFunction(TimeoutFunction f) -> void {
    timeout_function = f;
  }

  /// Get the maximum queue size
  inline auto maxQueuedActions() const noexcept -> std::size_t {
    return max_queued_actions;
  }

  /// Set the maximum number of queued actions
  ///
  /// Once the queue reaches the maximum size any new goals
  /// will be rejected.
  ///
  /// Using 0 disables the maximum size condition
  inline auto setMaxQueuedActions(std::size_t size) -> void {
    max_queued_actions = size;
  }

  /// Set the default timeout value
  inline auto setDefaultTimeout(ros::Duration timeout_) -> void {
    timeout = timeout_;
  }

  /// Get the default timeout value
  inline auto defaultTimeout() const noexcept -> ros::Duration {
    return timeout;
  }

private:

  auto queueNewGoalHandle(GoalHandle gh) -> void {
   auto result = Result{};
   if (!gh.getGoal()) {
     ROS_WARN_STREAM("tried to queue a null goal");
     gh.setRejected(result, "cannot handle null goal");
     return;
   }

   if (max_queued_actions > 0 && action_list.size() >= max_queued_actions) {
      gh.setRejected({}, "unable to queue new goal, maximum queue size reached.");
      return;
    }
    if (!validate(*gh.getGoal(), result)) {
      gh.setRejected(result, "unable to queue new goal, goal failed validation.");
      return;
    }

    action_list.push_back(gh);
    ROS_DEBUG_STREAM("added new goal to queue (" << action_list.size() << " total)");
    processNextGoal();
  }

  auto processNextGoal() -> void {
    if (action_list.empty()) {
      ROS_DEBUG_STREAM("no more queued goals."); 
      return;
    }
    else if (action_list.front().getGoalStatus().status != actionlib_msgs::GoalStatus::PENDING) {
      ROS_DEBUG_STREAM("next goal is already being processed, nothing to do.");
      return;
    }

    auto result = Result{};
    auto new_timeout = timeout;
    auto& gh = action_list.front();
    if (!gh.getGoal()) {
      ROS_ERROR_STREAM("next goal is null");
      gh.setAborted(result, "next goal is null");
      action_list.pop_front();
      processNextGoal();
    }
    else if (activate(*gh.getGoal(), result, new_timeout)) {
      gh.setAccepted();
      timer.stop();
      if (new_timeout.toSec() > 0.0) {
        timer.setPeriod(new_timeout);
        timer.start();
      }
    }
    else {
      ROS_WARN_STREAM("next goal failed to activate.");
      gh.setAborted(result, "next goal failed activation");
      action_list.pop_front();
      processNextGoal();
    }
  }

  auto cancelCallback(GoalHandle gh) -> void {
    const auto& id = gh.getGoalID();
    if (id.stamp.isZero() && id.id.empty()) {
      for(auto it = std::begin(action_list); it != std::end(action_list);) {
        it->setCanceled();
        it = action_list.erase(it);
      }
    }
    else {
      for(auto it = std::begin(action_list); it != std::end(action_list);) {
        auto action_id = it->getGoalID();
        if (action_id.stamp < id.stamp || action_id.id == id.id) {
        	it->setCanceled();
        	it = action_list.erase(it);
        }
        else {
        	++it;
        }
      }
    }
  }

auto timeoutCallback(const ros::TimerEvent&) -> void {
    if (goalIsActive()) {
      ROS_DEBUG_STREAM("timed out waiting for goal");
      auto gh = std::move(action_list.front());
      action_list.pop_front();
      auto result = Result{};
      timeout_function(*gh.getGoal(), result);
      gh.setAborted(result, "goal timed out");
      processNextGoal();
    }
    else {
      ROS_DEBUG_STREAM("timed out but no goal is active");
    }
  }


  actionlib::ActionServer<ActionSpec> server;
  std::list<GoalHandle> action_list;
  std::size_t max_queued_actions;
  
  ros::Timer timer;
  ros::Duration timeout = ros::Duration{5.0d};
  
  ValidationFunction validate = [](const Goal&, Result&) -> bool {return true;};
  TimeoutFunction timeout_function = [](const Goal&, Result&){};
  ActivationFunction activate = [](const Goal&, Result&, ros::Duration& timeout) -> bool {return true;};
};
}
#endif
